Table of Contents

[[_TOC_]]

---

## Domain model

### software

```plantuml
hide circle
hide methods

skinparam classAttributeIconSize 0
skinparam linetype polyline
skinparam linetype ortho
skinparam ClassBorderColor black
skinparam roundcorner 5

skinparam class {
  backgroundColor<<DomainAggregate>> cornflowerblue
  backgroundColor<<DomainEntity>> application
  backgroundColor<<ValueObject>> greenyellow
  backgroundColor<<Enumeration>> ivory
}

title software
right footer Copyright (c) NEXTREE Inc.\n@since 2014. 6. 10.

entity SoftwareProduct <<DomainAggregate>> <<DomainEntity>>

entity Price <<ValueObject>>
entity SoftwareType <<Enumeration>>

SoftwareProduct -r-> "price" Price
SoftwareProduct -l-> "type" SoftwareType
```

### category

```plantuml
hide circle
hide methods

skinparam classAttributeIconSize 0
skinparam linetype polyline
skinparam linetype ortho
skinparam ClassBorderColor black
skinparam roundcorner 5

skinparam class {
  backgroundColor<<DomainAggregate>> cornflowerblue
  backgroundColor<<DomainEntity>> application
  backgroundColor<<ValueObject>> greenyellow
  backgroundColor<<Enumeration>> ivory
}

title category
right footer Copyright (c) NEXTREE Inc.\n@since 2014. 6. 10.

entity Category <<DomainAggregate>> <<DomainEntity>> {}
```

## Api documentation

- [Product Web MVC with OpenAPI 3 @gcp](http://35.190.235.222/api/product/swagger-ui/index.html)
- [Product Web MVC with OpenAPI 3 @local](http://localhost:9050/swagger-ui/index.html)

## Persistence schema script

### local maria

```sql
use mysql;

create database product character set utf8 collate utf8_general_ci;

create user 'product'@'%' identified by 'product';
create user 'product'@'localhost' identified by 'product';

grant all privileges on product.* to product@'%';
grant all privileges on product.* to product@'localhost';

flush privileges;
```

### local mongo

```sql
use product

db.createUser(
  {
    user: "product",
    pwd: "product",
    roles : [
      { role: "readWrite", db: "product" }
    ]
  }
)
```

### gcp maria

```sql
use mysql;

create database product character set utf8 collate utf8_general_ci;

create user 'product'@'%' identified by 'product1234';
create user 'product'@'localhost' identified by 'product1234';

grant all privileges on product.* to product@'%';
grant all privileges on product.* to product@'localhost';

flush privileges;
```

### gcp mongo

```sql
use product

db.createUser(
  {
    user: "product",
    pwd: "product1234",
    roles : [
      { role: "readWrite", db: "product" }
    ]
  }
)
```

## Devops

```shell
# kubernetes secret 
kubectl create secret generic product-maria-secret --from-literal=datasource.password=product1234 -n naraway
kubectl create secret generic product-mongo-secret --from-literal=datasource.password=product1234 -n naraway
```
