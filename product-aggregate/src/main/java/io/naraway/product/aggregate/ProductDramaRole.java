/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */

package io.naraway.product.aggregate;

import io.naraway.accent.domain.key.kollection.DramaRole;
import io.naraway.accent.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.Assert;

import java.util.List;

@SuppressWarnings("java:S115")
@Getter
@Setter
@NoArgsConstructor
public class ProductDramaRole {
    //
    public static final String Director = "director";
    public static final String User = "user";

    private List<DramaRole> roles;

    public static void validate(String json) {
        //
        ProductDramaRole drama = JsonUtil.fromJson(json, ProductDramaRole.class);
        drama.validate();
    }

    public void validate() {
        //
        Assert.notNull(this.roles, "'roles' is required");

        if (roles.stream().noneMatch(role -> role.getCode().equals(Director))) {
            throw new IllegalArgumentException("drama role is missed, role = " + Director);
        }
        if (roles.stream().noneMatch(role -> role.getCode().equals(User))) {
            throw new IllegalArgumentException("drama role is missed, role = " + User);
        }
    }
}
