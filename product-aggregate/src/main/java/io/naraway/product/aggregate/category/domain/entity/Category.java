/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.aggregate.category.domain.entity;

import io.naraway.accent.domain.ddd.DomainAggregate;
import io.naraway.accent.domain.ddd.StageEntity;
import io.naraway.accent.domain.ddd.Updatable;
import io.naraway.accent.domain.key.stage.ActorKey;
import io.naraway.accent.domain.type.NameValue;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.product.aggregate.category.domain.entity.sdo.CategoryCdo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
@NoArgsConstructor
public class Category extends StageEntity implements DomainAggregate {
    //
    @Updatable
    private String name;

    public Category(String id, ActorKey requesterKey) {
        //
        super(id, requesterKey);
    }

    public Category(CategoryCdo categoryCdo) {
        //
        super(categoryCdo.genId(), categoryCdo.getRequesterKey());
        BeanUtils.copyProperties(categoryCdo, this);
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static Category fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Category.class);
    }

    @Override
    protected void modifyAttributes(NameValueList nameValues) {
        //
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "name":
                    this.name = value;
                    break;
                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }

    public static Category sample() {
        //
        return new Category(CategoryCdo.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
