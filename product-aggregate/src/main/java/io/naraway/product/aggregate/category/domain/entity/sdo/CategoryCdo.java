/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.aggregate.category.domain.entity.sdo;

import io.naraway.accent.util.json.JsonUtil;
import io.naraway.drama.prologue.domain.ddd.CreationDataObject;
import io.naraway.drama.prologue.spacekeeper.support.DramaRequestContext;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CategoryCdo extends CreationDataObject {
    //
    private String name;

    @Override
    public String genId() {
        //
        return super.genId();
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static CategoryCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, CategoryCdo.class);
    }

    public static CategoryCdo sample() {
        //
        DramaRequestContext.setSampleContext();

        return new CategoryCdo("SaaS");
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
