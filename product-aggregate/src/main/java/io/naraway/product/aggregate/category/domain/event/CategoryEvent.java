/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.category.domain.event;

import io.naraway.accent.domain.trail.DataEvent;
import io.naraway.accent.domain.trail.DataEventType;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.product.aggregate.category.domain.entity.Category;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CategoryEvent extends DataEvent {
    /* Autogen by nara studio */
    private Category category;
    private String categoryId;
    private NameValueList nameValues;

    protected CategoryEvent(DataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static CategoryEvent newCategoryRegisteredEvent(Category category, String categoryId) {
        /* Autogen by nara studio */
        CategoryEvent event = new CategoryEvent(DataEventType.Registered);
        event.setCategory(category);
        event.setCategoryId(categoryId);
        return event;
    }

    public static CategoryEvent newCategoryModifiedEvent(String categoryId, NameValueList nameValues, Category category) {
        /* Autogen by nara studio */
        CategoryEvent event = new CategoryEvent(DataEventType.Modified);
        event.setCategoryId(categoryId);
        event.setNameValues(nameValues);
        event.setCategory(category);
        return event;
    }

    public static CategoryEvent newCategoryRemovedEvent(Category category, String categoryId) {
        /* Autogen by nara studio */
        CategoryEvent event = new CategoryEvent(DataEventType.Removed);
        event.setCategory(category);
        event.setCategoryId(categoryId);
        return event;
    }

    public static CategoryEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, CategoryEvent.class);
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }
}
