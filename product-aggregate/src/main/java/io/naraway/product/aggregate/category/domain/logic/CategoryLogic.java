/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.category.domain.logic;

import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.entity.EntityUtil;
import io.naraway.janitor.proxy.EventProxy;
import io.naraway.product.aggregate.category.domain.entity.Category;
import io.naraway.product.aggregate.category.domain.entity.sdo.CategoryCdo;
import io.naraway.product.aggregate.category.domain.event.CategoryEvent;
import io.naraway.product.aggregate.category.store.CategoryStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@Transactional
public class CategoryLogic {
    //
    private final CategoryStore categoryStore;
    private final EventProxy eventProxy;

    public CategoryLogic(CategoryStore categoryStore, EventProxy eventProxy) {
        /* Autogen by nara studio */
        this.categoryStore = categoryStore;
        this.eventProxy = eventProxy;
    }

    public String registerCategory(CategoryCdo categoryCdo) {
        /* Autogen by nara studio */
        Category category = new Category(categoryCdo);
        if (categoryStore.exists(category.getId())) {
            throw new IllegalArgumentException("category already exists. " + category.getId());
        }
        categoryStore.create(category);
        CategoryEvent categoryEvent = CategoryEvent.newCategoryRegisteredEvent(category, category.getId());
        eventProxy.publishEvent(categoryEvent);
        return category.getId();
    }

    public List<String> registerCategorys(List<CategoryCdo> categoryCdos) {
        /* Autogen by nara studio */
        return categoryCdos.stream().map(categoryCdo -> this.registerCategory(categoryCdo)).collect(Collectors.toList());
    }

    public Category findCategory(String categoryId) {
        /* Autogen by nara studio */
        Category category = categoryStore.retrieve(categoryId);
        if (category == null) {
            throw new NoSuchElementException("Category id: " + categoryId);
        }
        return category;
    }

    public void modifyCategory(String categoryId, NameValueList nameValues) {
        /* Autogen by nara studio */
        Category category = findCategory(categoryId);
        category.modify(nameValues);
        categoryStore.update(category);
        CategoryEvent categoryEvent = CategoryEvent.newCategoryModifiedEvent(categoryId, nameValues, category);
        eventProxy.publishEvent(categoryEvent);
    }

    public void modifyCategory(Category category) {
        /* Autogen by nara studio */
        Category oldCategory = findCategory(category.getId());
        NameValueList nameValues = EntityUtil.genNameValueList(oldCategory, category);
        if (nameValues.size() > 0) {
            modifyCategory(category.getId(), nameValues);
        }
    }

    public void removeCategory(String categoryId) {
        /* Autogen by nara studio */
        Category category = findCategory(categoryId);
        categoryStore.delete(category);
        CategoryEvent categoryEvent = CategoryEvent.newCategoryRemovedEvent(category, category.getId());
        eventProxy.publishEvent(categoryEvent);
    }

    public boolean existsCategory(String categoryId) {
        /* Autogen by nara studio */
        return categoryStore.exists(categoryId);
    }

    public void handleEventForProjection(CategoryEvent categoryEvent) {
        /* Autogen by nara studio */
        switch (categoryEvent.getDataEventType()) {
            case Registered:
                categoryStore.create(categoryEvent.getCategory());
                break;
            case Modified:
                Category category = categoryStore.retrieve(categoryEvent.getCategoryId());
                category.modify(categoryEvent.getNameValues());
                categoryStore.update(category);
                break;
            case Removed:
                categoryStore.delete(categoryEvent.getCategoryId());
                break;
        }
    }
}
