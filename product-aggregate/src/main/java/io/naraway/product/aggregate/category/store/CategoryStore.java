/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.category.store;

import io.naraway.product.aggregate.category.domain.entity.Category;

public interface CategoryStore {
    /* Autogen by nara studio */
    void create(Category category);

    Category retrieve(String id);

    void update(Category category);

    void delete(Category category);

    void delete(String id);

    boolean exists(String id);
}
