/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.category.store.mongo;

import io.naraway.accent.domain.type.Offset;
import io.naraway.product.aggregate.category.domain.entity.Category;
import io.naraway.product.aggregate.category.store.CategoryStore;
import io.naraway.product.aggregate.category.store.mongo.odm.CategoryDoc;
import io.naraway.product.aggregate.category.store.mongo.repository.CategoryMongoRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class CategoryMongoStore implements CategoryStore {
    /* Autogen by nara studio */
    private final CategoryMongoRepository categoryMongoRepository;

    public CategoryMongoStore(CategoryMongoRepository categoryMongoRepository) {
        /* Autogen by nara studio */
        this.categoryMongoRepository = categoryMongoRepository;
    }

    @Override
    public void create(Category category) {
        /* Autogen by nara studio */
        CategoryDoc categoryDoc = new CategoryDoc(category);
        categoryMongoRepository.save(categoryDoc);
    }

    @Override
    public Category retrieve(String id) {
        /* Autogen by nara studio */
        Optional<CategoryDoc> categoryDoc = categoryMongoRepository.findById(id);
        return categoryDoc.map(CategoryDoc::toDomain).orElse(null);
    }

    @Override
    public void update(Category category) {
        /* Autogen by nara studio */
        CategoryDoc categoryDoc = new CategoryDoc(category);
        categoryMongoRepository.save(categoryDoc);
    }

    @Override
    public void delete(Category category) {
        /* Autogen by nara studio */
        categoryMongoRepository.deleteById(category.getId());
    }

    @Override
    public void delete(String id) {
        /* Autogen by nara studio */
        categoryMongoRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        /* Autogen by nara studio */
        return categoryMongoRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
