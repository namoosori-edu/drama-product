/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.category.store.mongo.odm;

import io.naraway.accent.store.mongo.StageEntityDoc;
import io.naraway.product.aggregate.category.domain.entity.Category;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "CATEGORY")
public class CategoryDoc extends StageEntityDoc {
    //
    private String name; // 

    public CategoryDoc(Category category) {
        /* Autogen by nara studio */
        super(category);
        BeanUtils.copyProperties(category, this);
    }

    public static List<Category> toDomains(List<CategoryDoc> categoryDocs) {
        /* Autogen by nara studio */
        return categoryDocs.stream().map(CategoryDoc::toDomain).collect(Collectors.toList());
    }

    public static CategoryDoc sample() {
        /* Autogen by nara studio */
        return new CategoryDoc(Category.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sample());
    }

    public Category toDomain() {
        /* Autogen by nara studio */
        Category category = new Category(getId(), genActorKey());
        BeanUtils.copyProperties(this, category);
        return category;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }
}
