/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.category.store.mongo.repository;

import io.naraway.product.aggregate.category.store.mongo.odm.CategoryDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CategoryMongoRepository extends MongoRepository<CategoryDoc, String> {
    /* Autogen by nara studio */
}
