/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.aggregate.software.domain.entity;

import io.naraway.accent.domain.ddd.DomainAggregate;
import io.naraway.accent.domain.ddd.StageEntity;
import io.naraway.accent.domain.ddd.Updatable;
import io.naraway.accent.domain.key.stage.ActorKey;
import io.naraway.accent.domain.type.NameValue;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.product.aggregate.software.domain.entity.sdo.SoftwareEditionCdo;
import io.naraway.product.aggregate.software.domain.entity.vo.Price;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SoftwareEdition extends StageEntity implements DomainAggregate {
    //
    @Updatable
    private String name;
    @Updatable
    private Price price;
    @Updatable
    private boolean discontinue;
    private String categoryId;

    transient private List<SoftwareEdition> products;

    public SoftwareEdition(String id, ActorKey requesterKey) {
        //
        super(id, requesterKey);
    }

    public SoftwareEdition(SoftwareEditionCdo softwareEditionCdo) {
        //
        super(softwareEditionCdo.genId(), softwareEditionCdo.getRequesterKey());
        BeanUtils.copyProperties(softwareEditionCdo, this);
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static SoftwareEdition fromJson(String json) {
        //
        return JsonUtil.fromJson(json, SoftwareEdition.class);
    }

    @Override
    protected void modifyAttributes(NameValueList nameValues) {
        //
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "name":
                    this.name = value;
                    break;
                case "discontinue":
                    this.discontinue = Boolean.parseBoolean(value);
                    break;
                case "price":
                    this.price = Price.fromJson(value);
                    break;
                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }

    public static SoftwareEdition sample() {
        //
        return new SoftwareEdition(SoftwareEditionCdo.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
