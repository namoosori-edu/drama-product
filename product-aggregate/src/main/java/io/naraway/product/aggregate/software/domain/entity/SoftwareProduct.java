/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.aggregate.software.domain.entity;

import io.naraway.accent.domain.ddd.DomainAggregate;
import io.naraway.accent.domain.ddd.Enum;
import io.naraway.accent.domain.ddd.StageEntity;
import io.naraway.accent.domain.ddd.Updatable;
import io.naraway.accent.domain.key.stage.ActorKey;
import io.naraway.accent.domain.type.NameValue;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.product.aggregate.software.domain.entity.sdo.SoftwareProductCdo;
import io.naraway.product.aggregate.software.domain.entity.vo.Price;
import io.naraway.product.aggregate.software.domain.entity.vo.SoftwareType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
@NoArgsConstructor
public class SoftwareProduct extends StageEntity implements DomainAggregate {
    //
    private String skuNo;
    @Updatable
    private String name;
    @Enum
    private SoftwareType type;
    @Updatable
    private Price price;
    @Updatable
    private boolean discontinue;
    private String categoryId;
    private String softwareEditionId;

    public SoftwareProduct(String id, ActorKey requesterKey) {
        //
        super(id, requesterKey);
    }

    public SoftwareProduct(SoftwareProductCdo softwareProductCdo) {
        //
        super(softwareProductCdo.genId(), softwareProductCdo.getRequesterKey());
        BeanUtils.copyProperties(softwareProductCdo, this);
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static SoftwareProduct fromJson(String json) {
        //
        return JsonUtil.fromJson(json, SoftwareProduct.class);
    }

    @Override
    protected void modifyAttributes(NameValueList nameValues) {
        //
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "name":
                    this.name = value;
                    break;
                case "discontinue":
                    this.discontinue = Boolean.parseBoolean(value);
                    break;
                case "price":
                    this.price = Price.fromJson(value);
                    break;
                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }

    public static SoftwareProduct sample() {
        //
        return new SoftwareProduct(SoftwareProductCdo.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
