/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.aggregate.software.domain.entity.sdo;

import io.naraway.accent.util.json.JsonUtil;
import io.naraway.drama.prologue.domain.ddd.CreationDataObject;
import io.naraway.drama.prologue.spacekeeper.support.DramaRequestContext;
import io.naraway.product.aggregate.category.domain.entity.Category;
import io.naraway.product.aggregate.software.domain.entity.SoftwareEdition;
import io.naraway.product.aggregate.software.domain.entity.vo.Price;
import io.naraway.product.aggregate.software.domain.entity.vo.SoftwareType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SoftwareProductCdo extends CreationDataObject {
    //
    private String skuNo;
    private String name;
    private SoftwareType type;
    private Price price;
    private boolean discontinue;
    private String categoryId;
    private String softwareEditionId;

    @Override
    public String genId() {
        //
        return this.skuNo;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static SoftwareProductCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, SoftwareProductCdo.class);
    }

    public static SoftwareProductCdo sample() {
        //
        DramaRequestContext.setSampleContext();

        return new SoftwareProductCdo(
                "190210000001",
                "Nomadian",
                SoftwareType.SubscriptionSoftware,
                Price.sample(),
                false,
                Category.sample().getId(),
                SoftwareEdition.sample().getId()
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
