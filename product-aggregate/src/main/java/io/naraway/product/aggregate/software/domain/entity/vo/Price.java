/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */

package io.naraway.product.aggregate.software.domain.entity.vo;

import io.naraway.accent.domain.ddd.ValueObject;
import io.naraway.accent.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Price implements ValueObject {
    //
    private long originalPrice;
    private long salesPrice;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static Price fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Price.class);
    }

    public static Price sample() {
        //
        return new Price(12000, 10000);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
