/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */

package io.naraway.product.aggregate.software.domain.entity.vo;

public enum SoftwareType {
    //
    PlatformSoftware,
    SharedSoftware,
    SaasSoftware,
    SubscriptionSoftware
}
