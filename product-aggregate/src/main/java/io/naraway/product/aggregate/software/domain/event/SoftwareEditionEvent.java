/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.software.domain.event;

import io.naraway.accent.domain.trail.DataEvent;
import io.naraway.accent.domain.trail.DataEventType;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.product.aggregate.software.domain.entity.SoftwareEdition;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SoftwareEditionEvent extends DataEvent {
    /* Autogen by nara studio */
    private SoftwareEdition softwareEdition;
    private String softwareEditionId;
    private NameValueList nameValues;

    protected SoftwareEditionEvent(DataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static SoftwareEditionEvent newSoftwareEditionRegisteredEvent(SoftwareEdition softwareEdition, String softwareEditionId) {
        /* Autogen by nara studio */
        SoftwareEditionEvent event = new SoftwareEditionEvent(DataEventType.Registered);
        event.setSoftwareEdition(softwareEdition);
        event.setSoftwareEditionId(softwareEditionId);
        return event;
    }

    public static SoftwareEditionEvent newSoftwareEditionModifiedEvent(String softwareEditionId, NameValueList nameValues, SoftwareEdition softwareEdition) {
        /* Autogen by nara studio */
        SoftwareEditionEvent event = new SoftwareEditionEvent(DataEventType.Modified);
        event.setSoftwareEditionId(softwareEditionId);
        event.setNameValues(nameValues);
        event.setSoftwareEdition(softwareEdition);
        return event;
    }

    public static SoftwareEditionEvent newSoftwareEditionRemovedEvent(SoftwareEdition softwareEdition, String softwareEditionId) {
        /* Autogen by nara studio */
        SoftwareEditionEvent event = new SoftwareEditionEvent(DataEventType.Removed);
        event.setSoftwareEdition(softwareEdition);
        event.setSoftwareEditionId(softwareEditionId);
        return event;
    }

    public static SoftwareEditionEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, SoftwareEditionEvent.class);
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }
}
