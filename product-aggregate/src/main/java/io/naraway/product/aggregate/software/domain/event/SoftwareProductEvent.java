/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.software.domain.event;

import io.naraway.accent.domain.trail.DataEvent;
import io.naraway.accent.domain.trail.DataEventType;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SoftwareProductEvent extends DataEvent {
    /* Autogen by nara studio */
    private SoftwareProduct softwareProduct;
    private String softwareProductId;
    private NameValueList nameValues;

    protected SoftwareProductEvent(DataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static SoftwareProductEvent newSoftwareProductRegisteredEvent(SoftwareProduct softwareProduct, String softwareProductId) {
        /* Autogen by nara studio */
        SoftwareProductEvent event = new SoftwareProductEvent(DataEventType.Registered);
        event.setSoftwareProduct(softwareProduct);
        event.setSoftwareProductId(softwareProductId);
        return event;
    }

    public static SoftwareProductEvent newSoftwareProductModifiedEvent(String softwareProductId, NameValueList nameValues, SoftwareProduct softwareProduct) {
        /* Autogen by nara studio */
        SoftwareProductEvent event = new SoftwareProductEvent(DataEventType.Modified);
        event.setSoftwareProductId(softwareProductId);
        event.setNameValues(nameValues);
        event.setSoftwareProduct(softwareProduct);
        return event;
    }

    public static SoftwareProductEvent newSoftwareProductRemovedEvent(SoftwareProduct softwareProduct, String softwareProductId) {
        /* Autogen by nara studio */
        SoftwareProductEvent event = new SoftwareProductEvent(DataEventType.Removed);
        event.setSoftwareProduct(softwareProduct);
        event.setSoftwareProductId(softwareProductId);
        return event;
    }

    public static SoftwareProductEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, SoftwareProductEvent.class);
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }
}
