/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.software.domain.logic;

import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.entity.EntityUtil;
import io.naraway.janitor.proxy.EventProxy;
import io.naraway.product.aggregate.software.domain.entity.SoftwareEdition;
import io.naraway.product.aggregate.software.domain.entity.sdo.SoftwareEditionCdo;
import io.naraway.product.aggregate.software.domain.event.SoftwareEditionEvent;
import io.naraway.product.aggregate.software.store.SoftwareEditionStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@Transactional
public class SoftwareEditionLogic {
    //
    private final SoftwareEditionStore softwareEditionStore;
    private final EventProxy eventProxy;

    public SoftwareEditionLogic(SoftwareEditionStore softwareEditionStore, EventProxy eventProxy) {
        /* Autogen by nara studio */
        this.softwareEditionStore = softwareEditionStore;
        this.eventProxy = eventProxy;
    }

    public String registerSoftwareEdition(SoftwareEditionCdo softwareEditionCdo) {
        /* Autogen by nara studio */
        SoftwareEdition softwareEdition = new SoftwareEdition(softwareEditionCdo);
        if (softwareEditionStore.exists(softwareEdition.getId())) {
            throw new IllegalArgumentException("softwareEdition already exists. " + softwareEdition.getId());
        }
        softwareEditionStore.create(softwareEdition);
        SoftwareEditionEvent softwareEditionEvent = SoftwareEditionEvent.newSoftwareEditionRegisteredEvent(softwareEdition, softwareEdition.getId());
        eventProxy.publishEvent(softwareEditionEvent);
        return softwareEdition.getId();
    }

    public List<String> registerSoftwareEditions(List<SoftwareEditionCdo> softwareEditionCdos) {
        /* Autogen by nara studio */
        return softwareEditionCdos.stream().map(softwareEditionCdo -> this.registerSoftwareEdition(softwareEditionCdo)).collect(Collectors.toList());
    }

    public SoftwareEdition findSoftwareEdition(String softwareEditionId) {
        /* Autogen by nara studio */
        SoftwareEdition softwareEdition = softwareEditionStore.retrieve(softwareEditionId);
        if (softwareEdition == null) {
            throw new NoSuchElementException("SoftwareEdition id: " + softwareEditionId);
        }
        return softwareEdition;
    }

    public void modifySoftwareEdition(String softwareEditionId, NameValueList nameValues) {
        /* Autogen by nara studio */
        SoftwareEdition softwareEdition = findSoftwareEdition(softwareEditionId);
        softwareEdition.modify(nameValues);
        softwareEditionStore.update(softwareEdition);
        SoftwareEditionEvent softwareEditionEvent = SoftwareEditionEvent.newSoftwareEditionModifiedEvent(softwareEditionId, nameValues, softwareEdition);
        eventProxy.publishEvent(softwareEditionEvent);
    }

    public void modifySoftwareEdition(SoftwareEdition softwareEdition) {
        /* Autogen by nara studio */
        SoftwareEdition oldSoftwareEdition = findSoftwareEdition(softwareEdition.getId());
        NameValueList nameValues = EntityUtil.genNameValueList(oldSoftwareEdition, softwareEdition);
        if (nameValues.size() > 0) {
            modifySoftwareEdition(softwareEdition.getId(), nameValues);
        }
    }

    public void removeSoftwareEdition(String softwareEditionId) {
        /* Autogen by nara studio */
        SoftwareEdition softwareEdition = findSoftwareEdition(softwareEditionId);
        softwareEditionStore.delete(softwareEdition);
        SoftwareEditionEvent softwareEditionEvent = SoftwareEditionEvent.newSoftwareEditionRemovedEvent(softwareEdition, softwareEdition.getId());
        eventProxy.publishEvent(softwareEditionEvent);
    }

    public boolean existsSoftwareEdition(String softwareEditionId) {
        /* Autogen by nara studio */
        return softwareEditionStore.exists(softwareEditionId);
    }

    public void handleEventForProjection(SoftwareEditionEvent softwareEditionEvent) {
        /* Autogen by nara studio */
        switch (softwareEditionEvent.getDataEventType()) {
            case Registered:
                softwareEditionStore.create(softwareEditionEvent.getSoftwareEdition());
                break;
            case Modified:
                SoftwareEdition softwareEdition = softwareEditionStore.retrieve(softwareEditionEvent.getSoftwareEditionId());
                softwareEdition.modify(softwareEditionEvent.getNameValues());
                softwareEditionStore.update(softwareEdition);
                break;
            case Removed:
                softwareEditionStore.delete(softwareEditionEvent.getSoftwareEditionId());
                break;
        }
    }
}
