/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.software.domain.logic;

import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.entity.EntityUtil;
import io.naraway.janitor.proxy.EventProxy;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import io.naraway.product.aggregate.software.domain.entity.sdo.SoftwareProductCdo;
import io.naraway.product.aggregate.software.domain.event.SoftwareProductEvent;
import io.naraway.product.aggregate.software.store.SoftwareProductStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@Transactional
public class SoftwareProductLogic {
    //
    private final SoftwareProductStore softwareProductStore; // 
    private final EventProxy eventProxy;

    public SoftwareProductLogic(SoftwareProductStore softwareProductStore, EventProxy eventProxy) {
        /* Autogen by nara studio */
        this.softwareProductStore = softwareProductStore;
        this.eventProxy = eventProxy;
    }

    public String registerSoftwareProduct(SoftwareProductCdo softwareProductCdo) {
        /* Autogen by nara studio */
        SoftwareProduct softwareProduct = new SoftwareProduct(softwareProductCdo);
        if (softwareProductStore.exists(softwareProduct.getId())) {
            throw new IllegalArgumentException("softwareProduct already exists. " + softwareProduct.getId());
        }
        softwareProductStore.create(softwareProduct);
        SoftwareProductEvent softwareProductEvent = SoftwareProductEvent.newSoftwareProductRegisteredEvent(softwareProduct, softwareProduct.getId());
        eventProxy.publishEvent(softwareProductEvent);
        return softwareProduct.getId();
    }

    public List<String> registerSoftwareProducts(List<SoftwareProductCdo> softwareProductCdos) {
        return /* Autogen by nara studio */
                softwareProductCdos.stream().map(softwareProductCdo -> this.registerSoftwareProduct(softwareProductCdo)).collect(Collectors.toList());
    }

    public SoftwareProduct findSoftwareProduct(String softwareProductId) {
        /* Autogen by nara studio */
        SoftwareProduct softwareProduct = softwareProductStore.retrieve(softwareProductId);
        if (softwareProduct == null) {
            throw new NoSuchElementException("SoftwareProduct id: " + softwareProductId);
        }
        return softwareProduct;
    }

    public void modifySoftwareProduct(String softwareProductId, NameValueList nameValues) {
        /* Autogen by nara studio */
        SoftwareProduct softwareProduct = findSoftwareProduct(softwareProductId);
        softwareProduct.modify(nameValues);
        softwareProductStore.update(softwareProduct);
        SoftwareProductEvent softwareProductEvent = SoftwareProductEvent.newSoftwareProductModifiedEvent(softwareProductId, nameValues, softwareProduct);
        eventProxy.publishEvent(softwareProductEvent);
    }

    public void modifySoftwareProduct(SoftwareProduct softwareProduct) {
        /* Autogen by nara studio */
        SoftwareProduct oldSoftwareProduct = findSoftwareProduct(softwareProduct.getId());
        NameValueList nameValues = EntityUtil.genNameValueList(oldSoftwareProduct, softwareProduct);
        if (nameValues.size() > 0) {
            modifySoftwareProduct(softwareProduct.getId(), nameValues);
        }
    }

    public void removeSoftwareProduct(String softwareProductId) {
        /* Autogen by nara studio */
        SoftwareProduct softwareProduct = findSoftwareProduct(softwareProductId);
        softwareProductStore.delete(softwareProduct);
        SoftwareProductEvent softwareProductEvent = SoftwareProductEvent.newSoftwareProductRemovedEvent(softwareProduct, softwareProduct.getId());
        eventProxy.publishEvent(softwareProductEvent);
    }

    public boolean existsSoftwareProduct(String softwareProductId) {
        return /* Autogen by nara studio */
                softwareProductStore.exists(softwareProductId);
    }

    public void handleEventForProjection(SoftwareProductEvent softwareProductEvent) {
        switch (/* Autogen by nara studio */
                softwareProductEvent.getDataEventType()) {
            case Registered:
                softwareProductStore.create(softwareProductEvent.getSoftwareProduct());
                break;
            case Modified:
                SoftwareProduct softwareProduct = softwareProductStore.retrieve(softwareProductEvent.getSoftwareProductId());
                softwareProduct.modify(softwareProductEvent.getNameValues());
                softwareProductStore.update(softwareProduct);
                break;
            case Removed:
                softwareProductStore.delete(softwareProductEvent.getSoftwareProductId());
                break;
        }
    }

    public SoftwareProduct findBySkuNo(String skuNo) {
        /* Autogen by nara studio */
        return softwareProductStore.retrieveBySkuNo(skuNo);
    }

    public List<SoftwareProduct> findByCategoryId(String categoryId) {
        /* Autogen by nara studio */
        return softwareProductStore.retrieveByCategoryId(categoryId);
    }
}
