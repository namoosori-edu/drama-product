/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.software.store;

import io.naraway.product.aggregate.software.domain.entity.SoftwareEdition;

public interface SoftwareEditionStore {
    /* Autogen by nara studio */
    void create(SoftwareEdition softwareEdition);

    SoftwareEdition retrieve(String id);

    void update(SoftwareEdition softwareEdition);

    void delete(SoftwareEdition softwareEdition);

    void delete(String id);

    boolean exists(String id);
}
