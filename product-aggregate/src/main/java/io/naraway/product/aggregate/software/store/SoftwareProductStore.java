/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.software.store;

import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;

import java.util.List;

public interface SoftwareProductStore {
    /* Autogen by nara studio */
    void create(SoftwareProduct softwareProduct);

    SoftwareProduct retrieve(String id);

    void update(SoftwareProduct softwareProduct);

    void delete(SoftwareProduct softwareProduct);

    void delete(String id);

    boolean exists(String id);

    SoftwareProduct retrieveBySkuNo(String skuNo);

    List<SoftwareProduct> retrieveByCategoryId(String categoryId);
}
