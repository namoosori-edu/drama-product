/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.software.store.mongo;

import io.naraway.accent.domain.type.Offset;
import io.naraway.product.aggregate.software.domain.entity.SoftwareEdition;
import io.naraway.product.aggregate.software.store.SoftwareEditionStore;
import io.naraway.product.aggregate.software.store.mongo.odm.SoftwareEditionDoc;
import io.naraway.product.aggregate.software.store.mongo.repository.SoftwareEditionMongoRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class SoftwareEditionMongoStore implements SoftwareEditionStore {
    /* Autogen by nara studio */
    private final SoftwareEditionMongoRepository softwareEditionMongoRepository;

    public SoftwareEditionMongoStore(SoftwareEditionMongoRepository softwareEditionMongoRepository) {
        /* Autogen by nara studio */
        this.softwareEditionMongoRepository = softwareEditionMongoRepository;
    }

    @Override
    public void create(SoftwareEdition softwareEdition) {
        /* Autogen by nara studio */
        SoftwareEditionDoc softwareEditionDoc = new SoftwareEditionDoc(softwareEdition);
        softwareEditionMongoRepository.save(softwareEditionDoc);
    }

    @Override
    public SoftwareEdition retrieve(String id) {
        /* Autogen by nara studio */
        Optional<SoftwareEditionDoc> softwareEditionDoc = softwareEditionMongoRepository.findById(id);
        return softwareEditionDoc.map(SoftwareEditionDoc::toDomain).orElse(null);
    }

    @Override
    public void update(SoftwareEdition softwareEdition) {
        /* Autogen by nara studio */
        SoftwareEditionDoc softwareEditionDoc = new SoftwareEditionDoc(softwareEdition);
        softwareEditionMongoRepository.save(softwareEditionDoc);
    }

    @Override
    public void delete(SoftwareEdition softwareEdition) {
        /* Autogen by nara studio */
        softwareEditionMongoRepository.deleteById(softwareEdition.getId());
    }

    @Override
    public void delete(String id) {
        /* Autogen by nara studio */
        softwareEditionMongoRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        /* Autogen by nara studio */
        return softwareEditionMongoRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
