/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.software.store.mongo;

import io.naraway.accent.domain.type.Offset;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import io.naraway.product.aggregate.software.store.SoftwareProductStore;
import io.naraway.product.aggregate.software.store.mongo.odm.SoftwareProductDoc;
import io.naraway.product.aggregate.software.store.mongo.repository.SoftwareProductMongoRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class SoftwareProductMongoStore implements SoftwareProductStore {
    /* Autogen by nara studio */
    private final SoftwareProductMongoRepository softwareProductMongoRepository;

    public SoftwareProductMongoStore(SoftwareProductMongoRepository softwareProductMongoRepository) {
        /* Autogen by nara studio */
        this.softwareProductMongoRepository = softwareProductMongoRepository;
    }

    @Override
    public void create(SoftwareProduct softwareProduct) {
        /* Autogen by nara studio */
        SoftwareProductDoc softwareProductDoc = new SoftwareProductDoc(softwareProduct);
        softwareProductMongoRepository.save(softwareProductDoc);
    }

    @Override
    public SoftwareProduct retrieve(String id) {
        /* Autogen by nara studio */
        Optional<SoftwareProductDoc> softwareProductDoc = softwareProductMongoRepository.findById(id);
        return softwareProductDoc.map(SoftwareProductDoc::toDomain).orElse(null);
    }

    @Override
    public void update(SoftwareProduct softwareProduct) {
        /* Autogen by nara studio */
        SoftwareProductDoc softwareProductDoc = new SoftwareProductDoc(softwareProduct);
        softwareProductMongoRepository.save(softwareProductDoc);
    }

    @Override
    public void delete(SoftwareProduct softwareProduct) {
        /* Autogen by nara studio */
        softwareProductMongoRepository.deleteById(softwareProduct.getId());
    }

    @Override
    public void delete(String id) {
        /* Autogen by nara studio */
        softwareProductMongoRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        /* Autogen by nara studio */
        return softwareProductMongoRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }

    @Override
    public SoftwareProduct retrieveBySkuNo(String skuNo) {
        /* Autogen by nara studio */
        return softwareProductMongoRepository.findBySkuNo(skuNo).map(SoftwareProductDoc::toDomain).orElse(null);
    }

    @Override
    public List<SoftwareProduct> retrieveByCategoryId(String categoryId) {
        /* Autogen by nara studio */
        return SoftwareProductDoc.toDomains(softwareProductMongoRepository.findByCategoryId(categoryId));
    }
}
