/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.software.store.mongo.odm;

import io.naraway.accent.store.mongo.StageEntityDoc;
import io.naraway.product.aggregate.software.domain.entity.SoftwareEdition;
import io.naraway.product.aggregate.software.domain.entity.vo.Price;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "SOFTWARE_EDITION")
public class SoftwareEditionDoc extends StageEntityDoc {
    //
    private String name; // 
    private Price price;
    private boolean discontinue;
    private String categoryId;

    public SoftwareEditionDoc(SoftwareEdition softwareEdition) {
        /* Autogen by nara studio */
        super(softwareEdition);
        BeanUtils.copyProperties(softwareEdition, this);
    }

    public static List<SoftwareEdition> toDomains(List<SoftwareEditionDoc> softwareEditionDocs) {
        /* Autogen by nara studio */
        return softwareEditionDocs.stream().map(SoftwareEditionDoc::toDomain).collect(Collectors.toList());
    }

    public static SoftwareEditionDoc sample() {
        /* Autogen by nara studio */
        return new SoftwareEditionDoc(SoftwareEdition.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sample());
    }

    public SoftwareEdition toDomain() {
        /* Autogen by nara studio */
        SoftwareEdition softwareEdition = new SoftwareEdition(getId(), genActorKey());
        BeanUtils.copyProperties(this, softwareEdition);
        return softwareEdition;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }
}
