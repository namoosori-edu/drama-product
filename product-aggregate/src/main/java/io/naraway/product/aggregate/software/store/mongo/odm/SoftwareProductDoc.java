/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.software.store.mongo.odm;

import io.naraway.accent.store.mongo.StageEntityDoc;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import io.naraway.product.aggregate.software.domain.entity.vo.Price;
import io.naraway.product.aggregate.software.domain.entity.vo.SoftwareType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "SOFTWARE_PRODUCT")
public class SoftwareProductDoc extends StageEntityDoc {
    //
    private String skuNo; // 
    private String name;
    private SoftwareType type;
    private Price price;
    private boolean discontinue;
    private String categoryId;
    private String softwareEditionId;

    public SoftwareProductDoc(SoftwareProduct softwareProduct) {
        /* Autogen by nara studio */
        super(softwareProduct);
        BeanUtils.copyProperties(softwareProduct, this);
    }

    public static List<SoftwareProduct> toDomains(List<SoftwareProductDoc> softwareProductDocs) {
        /* Autogen by nara studio */
        return softwareProductDocs.stream().map(SoftwareProductDoc::toDomain).collect(Collectors.toList());
    }

    public static SoftwareProductDoc sample() {
        /* Autogen by nara studio */
        return new SoftwareProductDoc(SoftwareProduct.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sample());
    }

    public SoftwareProduct toDomain() {
        /* Autogen by nara studio */
        SoftwareProduct softwareProduct = new SoftwareProduct(getId(), genActorKey());
        BeanUtils.copyProperties(this, softwareProduct);
        return softwareProduct;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }
}
