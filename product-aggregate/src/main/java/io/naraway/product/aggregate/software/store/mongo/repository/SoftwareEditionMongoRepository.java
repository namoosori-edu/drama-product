/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.software.store.mongo.repository;

import io.naraway.product.aggregate.software.store.mongo.odm.SoftwareEditionDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SoftwareEditionMongoRepository extends MongoRepository<SoftwareEditionDoc, String> {
    /* Autogen by nara studio */
}
