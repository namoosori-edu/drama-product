/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.aggregate.software.store.mongo.repository;

import io.naraway.product.aggregate.software.store.mongo.odm.SoftwareProductDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface SoftwareProductMongoRepository extends MongoRepository<SoftwareProductDoc, String> {
    /* Autogen by nara studio */
    Optional<SoftwareProductDoc> findBySkuNo(String skuNo);

    List<SoftwareProductDoc> findByCategoryId(String categoryId);
}
