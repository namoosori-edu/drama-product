/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product;

import io.naraway.drama.prologue.spacekeeper.config.DramaApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@DramaApplication
@SpringBootApplication(scanBasePackages = "io.naraway.product", exclude = DataSourceAutoConfiguration.class)
@EnableMongoRepositories("io.naraway.product")
public class ProductBootApplication {
    //
    public static void main(String[] args) {
        /* Autogen by nara studio */
        SpringApplication.run(ProductBootApplication.class, args);
    }
}
