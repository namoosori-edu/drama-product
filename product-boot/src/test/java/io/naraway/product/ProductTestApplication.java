/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product;

import io.naraway.drama.prologue.spacekeeper.config.DramaApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@DramaApplication
@SpringBootApplication(scanBasePackages = "io.naraway.product")
@EnableMongoRepositories("io.naraway.product")
public class ProductTestApplication {
    //
    public static void main(String[] args) {
        //
        SpringApplication.run(ProductTestApplication.class, args);
    }
}
