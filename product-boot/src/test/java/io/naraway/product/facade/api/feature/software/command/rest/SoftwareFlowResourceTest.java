package io.naraway.product.facade.api.feature.software.command.rest;

import io.naraway.accent.domain.trail.CommandResponse;
import io.naraway.product.ProductTestApplication;
import io.naraway.product.aggregate.category.domain.entity.sdo.CategoryCdo;
import io.naraway.product.aggregate.category.domain.logic.CategoryLogic;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import io.naraway.product.aggregate.software.domain.entity.sdo.SoftwareProductCdo;
import io.naraway.product.aggregate.software.domain.logic.SoftwareProductLogic;
import io.naraway.product.facade.api.feature.software.command.command.IncreaseSoftwareProductPriceCommand;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@SpringBootTest(
        classes = ProductTestApplication.class,
        properties = "spring.main.allow-bean-definition-overriding=true"
)
@ExtendWith(SpringExtension.class)
@DisplayName("Software flow resource")
class SoftwareFlowResourceTest {
    //
    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    SoftwareFlowResource softwareFlowResource;
    @Autowired
    CategoryLogic categoryLogic;
    @Autowired
    SoftwareProductLogic softwareProductLogic;

    @AfterEach
    void tearDown() {
        //
        mongoTemplate.getDb().listCollectionNames().forEach(name -> mongoTemplate.getDb().getCollection(name).drop());
    }

    @Test
    @DisplayName("Increase software produce sales price should same input rate")
    void increaseSoftwareProductPrice() {
        //

        // setup
        String categoryId = categoryLogic.registerCategory(CategoryCdo.sample());
        SoftwareProductCdo softwareProductCdo = SoftwareProductCdo.sample();
        softwareProductCdo.setCategoryId(categoryId);
        String skuNo = softwareProductCdo.getSkuNo();
        long price = softwareProductCdo.getPrice().getSalesPrice();
        softwareProductLogic.registerSoftwareProduct(softwareProductCdo);

        // command
        IncreaseSoftwareProductPriceCommand command = new IncreaseSoftwareProductPriceCommand(
                skuNo,
                1.1
        );
        log.debug("command = {}", command.toPrettyJson());
        CommandResponse response = softwareFlowResource.increaseSoftwareProductPrice(command);
        log.debug("response = {}", response.toPrettyJson());

        // assert
        assertEquals(1, response.getEntityIds().size());
        SoftwareProduct softwareProduct = softwareProductLogic.findBySkuNo(skuNo);
        assertEquals(price * 1.1, softwareProduct.getPrice().getSalesPrice());
    }
}
