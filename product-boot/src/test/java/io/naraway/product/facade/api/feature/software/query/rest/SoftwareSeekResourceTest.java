package io.naraway.product.facade.api.feature.software.query.rest;

import io.naraway.accent.domain.trail.QueryResponse;
import io.naraway.product.ProductTestApplication;
import io.naraway.product.aggregate.category.domain.entity.sdo.CategoryCdo;
import io.naraway.product.aggregate.category.domain.logic.CategoryLogic;
import io.naraway.product.aggregate.software.domain.entity.sdo.SoftwareProductCdo;
import io.naraway.product.aggregate.software.domain.logic.SoftwareProductLogic;
import io.naraway.product.facade.api.feature.software.query.query.FindSoftwareProductsByCategoryQuery;
import io.naraway.product.feature.software.domain.sdo.CategorizedSoftwareProductRdo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@SpringBootTest(
        classes = ProductTestApplication.class,
        properties = "spring.main.allow-bean-definition-overriding=true"
)
@ExtendWith(SpringExtension.class)
@DisplayName("Software seek resource")
class SoftwareSeekResourceTest {
    //
    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    SoftwareSeekResource softwareSeekResource;
    @Autowired
    CategoryLogic categoryLogic;
    @Autowired
    SoftwareProductLogic softwareProductLogic;

    @AfterEach
    void tearDown() {
        //
        mongoTemplate.getDb().listCollectionNames().forEach(name -> mongoTemplate.getDb().getCollection(name).drop());
    }

    @Test
    @DisplayName("Find product by categoryId should exists")
    void findProductByCategoryId() {
        //

        // setup
        String categoryId = categoryLogic.registerCategory(CategoryCdo.sample());
        SoftwareProductCdo softwareProductCdo = SoftwareProductCdo.sample();
        softwareProductCdo.setCategoryId(categoryId);
        String skuNo = softwareProductCdo.getSkuNo();
        softwareProductLogic.registerSoftwareProduct(softwareProductCdo);

        // query
        FindSoftwareProductsByCategoryQuery query = new FindSoftwareProductsByCategoryQuery(
                categoryId
        );
        log.debug("query = {}", query.toPrettyJson());
        QueryResponse<List<CategorizedSoftwareProductRdo>> response = softwareSeekResource.findSoftwareProductsByCategory(query);
        log.debug("response = {}", response.toPrettyJson());

        // assert
        assertEquals(1, response.getQueryResult().size());
        assertTrue(response.getQueryResult().stream()
                .allMatch(softwareProduct -> softwareProduct.getCategory().getId().equals(categoryId)));
    }
}
