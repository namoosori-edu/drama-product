/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.client;

import io.naraway.product.client.sdo.software.CategorizedSoftwareProductRdo;

import java.util.List;

public interface SoftwareProductClient {
    //
    List<CategorizedSoftwareProductRdo> findSoftwareProductsByCategory(String categoryId);
}
