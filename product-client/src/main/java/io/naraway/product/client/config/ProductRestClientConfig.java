/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.client.config;

import io.naraway.drama.prologue.spacekeeper.support.DramaHeaderConsumerProvider;
import io.naraway.product.client.config.condition.ProductRestClientCondition;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@Conditional(ProductRestClientCondition.class)
@Slf4j
public class ProductRestClientConfig {
    //
    @Bean
    @Qualifier("productWebClient")
    public WebClient getMetroWebClient(
            @Value("${nara.product.url:http://product:8080}") String productUrl,
            @Value("${nara.product.max-memory-size:104857600}") int maxMemorySize
    ) {
        //
        String baseUrl = productUrl;
        log.debug("productUrl : {}", baseUrl);

        ExchangeStrategies exchangeStrategies = ExchangeStrategies.builder()
                .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(maxMemorySize))
                .build();

        return WebClient
                .builder()
                .baseUrl(baseUrl)
                .exchangeStrategies(exchangeStrategies)
                .build();
    }

    @Bean
    @ConditionalOnMissingBean
    public PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        //
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    @ConditionalOnMissingBean
    public DramaHeaderConsumerProvider productHeaderConsumerProvider() {
        //
        return new DramaHeaderConsumerProvider();
    }
}
