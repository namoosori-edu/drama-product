/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */

package io.naraway.product.client.config.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Optional;

public class ProductLocalClientCondition implements Condition {
    //
    private static final String MODE = "local";

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        //
        Environment env = context.getEnvironment();
        String mode = env.getProperty("nara.product.mode");

        return Optional.ofNullable(mode)
                .map(m -> m.equals(MODE))
                .orElse(false);
    }
}
