/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.client.local;

import io.naraway.product.client.SoftwareProductClient;
import io.naraway.product.client.config.condition.ProductLocalClientCondition;
import io.naraway.product.client.sdo.software.CategorizedSoftwareProductRdo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
@Conditional(ProductLocalClientCondition.class)
public class SoftwareProductLocalClient implements SoftwareProductClient {
    //
    @Override
    public List<CategorizedSoftwareProductRdo> findSoftwareProductsByCategory(String categoryId) {
        //
        log.info("findSoftwareProductsByCategory, categoryId = {}", categoryId);
        return Arrays.asList(CategorizedSoftwareProductRdo.sample());
    }
}
