/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.client.rest;

import io.naraway.accent.domain.trail.QueryRequest;
import io.naraway.accent.domain.trail.QueryResponse;
import io.naraway.drama.prologue.spacekeeper.support.DramaHeaderConsumerProvider;
import io.naraway.product.client.SoftwareProductClient;
import io.naraway.product.client.config.condition.ProductRestClientCondition;
import io.naraway.product.client.sdo.software.CategorizedSoftwareProductRdo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Consumer;

@Component
@Conditional(ProductRestClientCondition.class)
@Slf4j
public class SoftwareProductRestClient implements SoftwareProductClient {
    //
    private final WebClient webClient;
    private final Consumer<HttpHeaders> headersConsumer;

    public SoftwareProductRestClient(@Qualifier("productWebClient") WebClient webClient,
                                     DramaHeaderConsumerProvider headerConsumerProvider) {
        //
        this.webClient = webClient;
        this.headersConsumer = headerConsumerProvider.getConsumer();
    }

    @Override
    public List<CategorizedSoftwareProductRdo> findSoftwareProductsByCategory(String categoryId) {
        //
        log.info("findSoftwareProductsByCategory, categoryId = {}", categoryId);
        FindSoftwareProductsByCategoryQuery query = new FindSoftwareProductsByCategoryQuery(categoryId);

        QueryResponse<List<CategorizedSoftwareProductRdo>> result = webClient.post()
                .uri("/feature/software/find-software-products-by-category-id/query")
                .headers(headersConsumer)
                .body(BodyInserters.fromValue(query))
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<QueryResponse<List<CategorizedSoftwareProductRdo>>>() {
                })
                .block();

        if (result == null || result.getQueryResult() == null) {
            throw new NoSuchElementException("categoryId = " + categoryId);
        }

        return result.getQueryResult();
    }

    // domain reference model

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public class FindSoftwareProductsByCategoryQuery extends QueryRequest<List<CategorizedSoftwareProductRdo>> {
        //
        private String categoryId;
    }
}
