/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.client.sdo.category;

import io.naraway.accent.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@Slf4j
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Category {
    //
    private String id;
    private String name;

    public static Category sample() {
        //
        return new Category(
                UUID.randomUUID().toString(),
                "Saas"
        );
    }

    public static void main(String[] args) {
        //
        log.debug(JsonUtil.toPrettyJson(sample()));
    }
}
