/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.client.sdo.software;

import io.naraway.accent.util.json.JsonUtil;
import io.naraway.product.client.sdo.category.Category;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CategorizedSoftwareProductRdo {
    //
    private SoftwareProduct softwareProduct;
    private Category category;

    public static CategorizedSoftwareProductRdo sample() {
        //
        return new CategorizedSoftwareProductRdo(SoftwareProduct.sample(), Category.sample());
    }

    public static void main(String[] args) {
        //
        log.debug(JsonUtil.toPrettyJson(sample()));
    }
}
