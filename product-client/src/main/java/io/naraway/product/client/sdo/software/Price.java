/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.client.sdo.software;

import io.naraway.accent.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Price {
    //
    private long originalPrice;
    private long salesPrice;

    public static Price sample() {
        //
        return new Price(12000, 10000);
    }

    public static void main(String[] args) {
        //
        log.debug(JsonUtil.toPrettyJson(sample()));
    }
}
