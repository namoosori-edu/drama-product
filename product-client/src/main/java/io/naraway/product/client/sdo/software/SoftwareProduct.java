/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.client.sdo.software;

import io.naraway.accent.domain.ddd.Enum;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.product.client.sdo.category.Category;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@Slf4j
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SoftwareProduct {
    //
    private String id;
    private String skuNo;
    private String name;
    @Enum
    private SoftwareType type;
    private Price price;
    private boolean discontinue;
    private String categoryId;

    public static SoftwareProduct sample() {
        //
        return new SoftwareProduct(
                UUID.randomUUID().toString(),
                "190210000001",
                "Nomadian",
                SoftwareType.SubscriptionSoftware,
                Price.sample(),
                false,
                Category.sample().getId()
        );
    }

    public static void main(String[] args) {
        //
        log.debug(JsonUtil.toPrettyJson(sample()));
    }
}
