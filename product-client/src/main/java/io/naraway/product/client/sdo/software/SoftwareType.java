/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.client.sdo.software;

@SuppressWarnings("java:S115")
public enum SoftwareType {
    //
    PlatformSoftware,
    SharedSoftware,
    SaasSoftware,
    SubscriptionSoftware
}
