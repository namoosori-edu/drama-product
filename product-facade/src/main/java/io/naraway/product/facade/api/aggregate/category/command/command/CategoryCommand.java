/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.category.command.command;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.CommandRequest;
import io.naraway.accent.domain.trail.CommandType;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.product.aggregate.ProductDramaRole;
import io.naraway.product.aggregate.category.domain.entity.sdo.CategoryCdo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(ProductDramaRole.Director)
public class CategoryCommand extends CommandRequest {
    //
    private CategoryCdo categoryCdo;
    private List<CategoryCdo> categoryCdos;
    private boolean multiCdo;
    private String categoryId;
    private NameValueList nameValues;

    protected CategoryCommand(CommandType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static CategoryCommand newRegisterCategoryCommand(CategoryCdo categoryCdo) {
        /* Autogen by nara studio */
        CategoryCommand command = new CategoryCommand(CommandType.Register);
        command.setCategoryCdo(categoryCdo);
        return command;
    }

    public static CategoryCommand newRegisterCategoryCommand(List<CategoryCdo> categoryCdos) {
        /* Autogen by nara studio */
        CategoryCommand command = new CategoryCommand(CommandType.Register);
        command.setCategoryCdos(categoryCdos);
        command.setMultiCdo(true);
        return command;
    }

    public static CategoryCommand newModifyCategoryCommand(String categoryId, NameValueList nameValues) {
        /* Autogen by nara studio */
        CategoryCommand command = new CategoryCommand(CommandType.Modify);
        command.setCategoryId(categoryId);
        command.setNameValues(nameValues);
        return command;
    }

    public static CategoryCommand newRemoveCategoryCommand(String categoryId) {
        /* Autogen by nara studio */
        CategoryCommand command = new CategoryCommand(CommandType.Remove);
        command.setCategoryId(categoryId);
        return command;
    }

    public static CategoryCommand fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, CategoryCommand.class);
    }

    public static CategoryCommand sampleForRegister() {
        /* Autogen by nara studio */
        return newRegisterCategoryCommand(CategoryCdo.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sampleForRegister());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }
}
