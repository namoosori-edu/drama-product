/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.category.command.rest;

import io.naraway.accent.domain.trail.CommandResponse;
import io.naraway.product.facade.api.aggregate.category.command.command.CategoryCommand;

public interface CategoryFacade {
    /* Autogen by nara studio */
    CommandResponse executeCategory(CategoryCommand categoryCommand);
}
