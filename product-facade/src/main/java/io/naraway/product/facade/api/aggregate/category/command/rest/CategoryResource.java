/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.category.command.rest;

import io.naraway.accent.domain.trail.CommandResponse;
import io.naraway.accent.domain.trail.FailureMessage;
import io.naraway.product.aggregate.category.domain.logic.CategoryLogic;
import io.naraway.product.facade.api.aggregate.category.command.command.CategoryCommand;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/aggregate/category")
public class CategoryResource implements CategoryFacade {
    /* Autogen by nara studio */
    private final CategoryLogic categoryLogic;

    public CategoryResource(CategoryLogic categoryLogic) {
        /* Autogen by nara studio */
        this.categoryLogic = categoryLogic;
    }

    @Override
    @PostMapping("/category/command")
    public CommandResponse executeCategory(@RequestBody CategoryCommand categoryCommand) {
        /* Autogen by nara studio */
        return routeCommand(categoryCommand).getResponse();
    }

    private CategoryCommand routeCommand(CategoryCommand command) {
        /* Autogen by nara studio */
        switch (command.getCommandType()) {
            case Register:
                if (command.isMultiCdo()) {
                    List<String> entityIds = categoryLogic.registerCategorys(command.getCategoryCdos());
                    command.setResponse(entityIds);
                } else {
                    String entityId = categoryLogic.registerCategory(command.getCategoryCdo());
                    command.setResponse(entityId);
                }
                break;
            case Modify:
                categoryLogic.modifyCategory(command.getCategoryId(), command.getNameValues());
                command.setResponse(command.getCategoryId());
                break;
            case Remove:
                categoryLogic.removeCategory(command.getCategoryId());
                command.setResponse(command.getCategoryId());
                break;
            default:
                command.setResponse(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }
}
