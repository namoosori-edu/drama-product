/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.category.query.query;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.DynamicQueryRequest;
import io.naraway.accent.util.query.DocQueryBuilder;
import io.naraway.accent.util.query.DocQueryRequest;
import io.naraway.product.aggregate.ProductDramaRole;
import io.naraway.product.aggregate.category.domain.entity.Category;
import io.naraway.product.aggregate.category.store.mongo.odm.CategoryDoc;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(ProductDramaRole.Director)
public class CategoryDynamicQuery extends DynamicQueryRequest<Category> {
    /* Autogen by nara studio */

    public void execute(DocQueryRequest<CategoryDoc> request) {
        /* Autogen by nara studio */
        request.addQueryParamsAndClass(getQueryParams(), CategoryDoc.class);
        Query query = DocQueryBuilder.build(request);
        CategoryDoc categoryDoc = request.findOne(query);
        setResponse(Optional.ofNullable(categoryDoc).map(doc -> doc.toDomain()).orElse(null));
    }
}
