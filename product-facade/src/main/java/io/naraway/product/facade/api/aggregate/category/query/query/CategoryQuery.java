/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.category.query.query;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.QueryRequest;
import io.naraway.product.aggregate.ProductDramaRole;
import io.naraway.product.aggregate.category.domain.entity.Category;
import io.naraway.product.aggregate.category.store.CategoryStore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(ProductDramaRole.Director)
public class CategoryQuery extends QueryRequest<Category> {
    /* Autogen by nara studio */
    private String categoryId;

    public void execute(CategoryStore categoryStore) {
        /* Autogen by nara studio */
        setResponse(categoryStore.retrieve(categoryId));
    }
}
