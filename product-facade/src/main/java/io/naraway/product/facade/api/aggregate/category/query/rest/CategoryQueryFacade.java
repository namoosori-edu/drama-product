/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.category.query.rest;

import io.naraway.accent.domain.trail.QueryResponse;
import io.naraway.product.aggregate.category.domain.entity.Category;
import io.naraway.product.facade.api.aggregate.category.query.query.CategoryDynamicQuery;
import io.naraway.product.facade.api.aggregate.category.query.query.CategoryQuery;
import io.naraway.product.facade.api.aggregate.category.query.query.CategorysDynamicQuery;

import java.util.List;

public interface CategoryQueryFacade {
    /* Autogen by nara studio */
    QueryResponse<Category> execute(CategoryQuery categoryQuery);

    QueryResponse<Category> execute(CategoryDynamicQuery categoryDynamicQuery);

    QueryResponse<List<Category>> execute(CategorysDynamicQuery categorysDynamicQuery);
}
