/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.category.query.rest;

import io.naraway.accent.domain.trail.QueryResponse;
import io.naraway.accent.util.query.DocQueryRequest;
import io.naraway.product.aggregate.category.domain.entity.Category;
import io.naraway.product.aggregate.category.store.CategoryStore;
import io.naraway.product.aggregate.category.store.mongo.odm.CategoryDoc;
import io.naraway.product.facade.api.aggregate.category.query.query.CategoryDynamicQuery;
import io.naraway.product.facade.api.aggregate.category.query.query.CategoryQuery;
import io.naraway.product.facade.api.aggregate.category.query.query.CategorysDynamicQuery;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/aggregate/category")
public class CategoryQueryResource implements CategoryQueryFacade {
    /* Autogen by nara studio */
    private final CategoryStore categoryStore;
    private final MongoTemplate mongoTemplate;

    public CategoryQueryResource(CategoryStore categoryStore, MongoTemplate mongoTemplate) {
        /* Autogen by nara studio */
        this.categoryStore = categoryStore;
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    @PostMapping("/category/query")
    public QueryResponse<Category> execute(@RequestBody CategoryQuery categoryQuery) {
        /* Autogen by nara studio */
        categoryQuery.execute(categoryStore);
        return categoryQuery.getResponse();
    }

    @Override
    @PostMapping("/category/dynamic-single/query")
    public QueryResponse<Category> execute(@RequestBody CategoryDynamicQuery categoryDynamicQuery) {
        /* Autogen by nara studio */
        DocQueryRequest<CategoryDoc> request = new DocQueryRequest<>(mongoTemplate);
        categoryDynamicQuery.execute(request);
        return categoryDynamicQuery.getResponse();
    }

    @Override
    @PostMapping("/category/dynamic-multi/query")
    public QueryResponse<List<Category>> execute(@RequestBody CategorysDynamicQuery categorysDynamicQuery) {
        /* Autogen by nara studio */
        DocQueryRequest<CategoryDoc> request = new DocQueryRequest<>(mongoTemplate);
        categorysDynamicQuery.execute(request);
        return categorysDynamicQuery.getResponse();
    }
}
