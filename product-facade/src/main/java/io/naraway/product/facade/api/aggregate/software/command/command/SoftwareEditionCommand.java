/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.software.command.command;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.CommandRequest;
import io.naraway.accent.domain.trail.CommandType;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.product.aggregate.ProductDramaRole;
import io.naraway.product.aggregate.software.domain.entity.sdo.SoftwareEditionCdo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(ProductDramaRole.Director)
public class SoftwareEditionCommand extends CommandRequest {
    //
    private SoftwareEditionCdo softwareEditionCdo;
    private List<SoftwareEditionCdo> softwareEditionCdos;
    private boolean multiCdo;
    private String softwareEditionId;
    private NameValueList nameValues;

    protected SoftwareEditionCommand(CommandType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static SoftwareEditionCommand newRegisterSoftwareEditionCommand(SoftwareEditionCdo softwareEditionCdo) {
        /* Autogen by nara studio */
        SoftwareEditionCommand command = new SoftwareEditionCommand(CommandType.Register);
        command.setSoftwareEditionCdo(softwareEditionCdo);
        return command;
    }

    public static SoftwareEditionCommand newRegisterSoftwareEditionCommand(List<SoftwareEditionCdo> softwareEditionCdos) {
        /* Autogen by nara studio */
        SoftwareEditionCommand command = new SoftwareEditionCommand(CommandType.Register);
        command.setSoftwareEditionCdos(softwareEditionCdos);
        command.setMultiCdo(true);
        return command;
    }

    public static SoftwareEditionCommand newModifySoftwareEditionCommand(String softwareEditionId, NameValueList nameValues) {
        /* Autogen by nara studio */
        SoftwareEditionCommand command = new SoftwareEditionCommand(CommandType.Modify);
        command.setSoftwareEditionId(softwareEditionId);
        command.setNameValues(nameValues);
        return command;
    }

    public static SoftwareEditionCommand newRemoveSoftwareEditionCommand(String softwareEditionId) {
        /* Autogen by nara studio */
        SoftwareEditionCommand command = new SoftwareEditionCommand(CommandType.Remove);
        command.setSoftwareEditionId(softwareEditionId);
        return command;
    }

    public static SoftwareEditionCommand fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, SoftwareEditionCommand.class);
    }

    public static SoftwareEditionCommand sampleForRegister() {
        /* Autogen by nara studio */
        return newRegisterSoftwareEditionCommand(SoftwareEditionCdo.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sampleForRegister());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }
}
