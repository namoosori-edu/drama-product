/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.software.command.command;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.CommandRequest;
import io.naraway.accent.domain.trail.CommandType;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.product.aggregate.ProductDramaRole;
import io.naraway.product.aggregate.software.domain.entity.sdo.SoftwareProductCdo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(ProductDramaRole.Director)
public class SoftwareProductCommand extends CommandRequest {
    //
    private SoftwareProductCdo softwareProductCdo;
    private List<SoftwareProductCdo> softwareProductCdos;
    private boolean multiCdo;
    private String softwareProductId;
    private NameValueList nameValues;

    protected SoftwareProductCommand(CommandType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static SoftwareProductCommand newRegisterSoftwareProductCommand(SoftwareProductCdo softwareProductCdo) {
        /* Autogen by nara studio */
        SoftwareProductCommand command = new SoftwareProductCommand(CommandType.Register);
        command.setSoftwareProductCdo(softwareProductCdo);
        return command;
    }

    public static SoftwareProductCommand newRegisterSoftwareProductCommand(List<SoftwareProductCdo> softwareProductCdos) {
        /* Autogen by nara studio */
        SoftwareProductCommand command = new SoftwareProductCommand(CommandType.Register);
        command.setSoftwareProductCdos(softwareProductCdos);
        command.setMultiCdo(true);
        return command;
    }

    public static SoftwareProductCommand newModifySoftwareProductCommand(String softwareProductId, NameValueList nameValues) {
        /* Autogen by nara studio */
        SoftwareProductCommand command = new SoftwareProductCommand(CommandType.Modify);
        command.setSoftwareProductId(softwareProductId);
        command.setNameValues(nameValues);
        return command;
    }

    public static SoftwareProductCommand newRemoveSoftwareProductCommand(String softwareProductId) {
        /* Autogen by nara studio */
        SoftwareProductCommand command = new SoftwareProductCommand(CommandType.Remove);
        command.setSoftwareProductId(softwareProductId);
        return command;
    }

    public static SoftwareProductCommand fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, SoftwareProductCommand.class);
    }

    public static SoftwareProductCommand sampleForRegister() {
        /* Autogen by nara studio */
        return newRegisterSoftwareProductCommand(SoftwareProductCdo.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sampleForRegister());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }
}
