/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.software.command.rest;

import io.naraway.accent.domain.trail.CommandResponse;
import io.naraway.product.facade.api.aggregate.software.command.command.SoftwareEditionCommand;
import io.naraway.product.facade.api.aggregate.software.command.command.SoftwareProductCommand;

public interface SoftwareFacade {
    /* Autogen by nara studio */
    CommandResponse executeSoftwareProduct(SoftwareProductCommand softwareProductCommand);

    CommandResponse executeSoftwareEdition(SoftwareEditionCommand softwareEditionCommand);
}
