/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.software.command.rest;

import io.naraway.accent.domain.trail.CommandResponse;
import io.naraway.accent.domain.trail.FailureMessage;
import io.naraway.product.aggregate.software.domain.logic.SoftwareEditionLogic;
import io.naraway.product.aggregate.software.domain.logic.SoftwareProductLogic;
import io.naraway.product.facade.api.aggregate.software.command.command.SoftwareEditionCommand;
import io.naraway.product.facade.api.aggregate.software.command.command.SoftwareProductCommand;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/aggregate/software")
public class SoftwareResource implements SoftwareFacade {
    /* Autogen by nara studio */
    private final SoftwareProductLogic softwareProductLogic;
    private final SoftwareEditionLogic softwareEditionLogic;

    public SoftwareResource(SoftwareProductLogic softwareProductLogic, SoftwareEditionLogic softwareEditionLogic) {
        /* Autogen by nara studio */
        this.softwareProductLogic = softwareProductLogic;
        this.softwareEditionLogic = softwareEditionLogic;
    }

    @Override
    @PostMapping("/software-product/command")
    public CommandResponse executeSoftwareProduct(@RequestBody SoftwareProductCommand softwareProductCommand) {
        /* Autogen by nara studio */
        return routeCommand(softwareProductCommand).getResponse();
    }

    @Override
    @PostMapping("/software-edition/command")
    public CommandResponse executeSoftwareEdition(@RequestBody SoftwareEditionCommand softwareEditionCommand) {
        return /* Autogen by nara studio */
                routeCommand(softwareEditionCommand).getResponse();
    }

    private SoftwareProductCommand routeCommand(SoftwareProductCommand command) {
        /* Autogen by nara studio */
        switch (command.getCommandType()) {
            case Register:
                if (command.isMultiCdo()) {
                    List<String> entityIds = softwareProductLogic.registerSoftwareProducts(command.getSoftwareProductCdos());
                    command.setResponse(entityIds);
                } else {
                    String entityId = softwareProductLogic.registerSoftwareProduct(command.getSoftwareProductCdo());
                    command.setResponse(entityId);
                }
                break;
            case Modify:
                softwareProductLogic.modifySoftwareProduct(command.getSoftwareProductId(), command.getNameValues());
                command.setResponse(command.getSoftwareProductId());
                break;
            case Remove:
                softwareProductLogic.removeSoftwareProduct(command.getSoftwareProductId());
                command.setResponse(command.getSoftwareProductId());
                break;
            default:
                command.setResponse(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    private SoftwareEditionCommand routeCommand(SoftwareEditionCommand command) {
        switch (/* Autogen by nara studio */
                command.getCommandType()) {
            case Register:
                if (command.isMultiCdo()) {
                    List<String> entityIds = softwareEditionLogic.registerSoftwareEditions(command.getSoftwareEditionCdos());
                    command.setResponse(entityIds);
                } else {
                    String entityId = softwareEditionLogic.registerSoftwareEdition(command.getSoftwareEditionCdo());
                    command.setResponse(entityId);
                }
                break;
            case Modify:
                softwareEditionLogic.modifySoftwareEdition(command.getSoftwareEditionId(), command.getNameValues());
                command.setResponse(command.getSoftwareEditionId());
                break;
            case Remove:
                softwareEditionLogic.removeSoftwareEdition(command.getSoftwareEditionId());
                command.setResponse(command.getSoftwareEditionId());
                break;
            default:
                command.setResponse(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }
}
