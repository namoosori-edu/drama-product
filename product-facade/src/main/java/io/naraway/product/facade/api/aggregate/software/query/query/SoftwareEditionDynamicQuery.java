/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.software.query.query;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.DynamicQueryRequest;
import io.naraway.accent.util.query.DocQueryBuilder;
import io.naraway.accent.util.query.DocQueryRequest;
import io.naraway.product.aggregate.ProductDramaRole;
import io.naraway.product.aggregate.software.domain.entity.SoftwareEdition;
import io.naraway.product.aggregate.software.store.mongo.odm.SoftwareEditionDoc;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(ProductDramaRole.Director)
public class SoftwareEditionDynamicQuery extends DynamicQueryRequest<SoftwareEdition> {
    /* Autogen by nara studio */

    public void execute(DocQueryRequest<SoftwareEditionDoc> request) {
        /* Autogen by nara studio */
        request.addQueryParamsAndClass(getQueryParams(), SoftwareEditionDoc.class);
        Query query = DocQueryBuilder.build(request);
        SoftwareEditionDoc softwareEditionDoc = request.findOne(query);
        setResponse(Optional.ofNullable(softwareEditionDoc).map(doc -> doc.toDomain()).orElse(null));
    }
}
