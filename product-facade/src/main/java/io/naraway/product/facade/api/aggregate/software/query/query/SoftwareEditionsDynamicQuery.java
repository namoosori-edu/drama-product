/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.software.query.query;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.DynamicQueryRequest;
import io.naraway.accent.domain.type.Offset;
import io.naraway.accent.util.query.DocQueryBuilder;
import io.naraway.accent.util.query.DocQueryRequest;
import io.naraway.product.aggregate.ProductDramaRole;
import io.naraway.product.aggregate.software.domain.entity.SoftwareEdition;
import io.naraway.product.aggregate.software.store.mongo.odm.SoftwareEditionDoc;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(ProductDramaRole.Director)
public class SoftwareEditionsDynamicQuery extends DynamicQueryRequest<List<SoftwareEdition>> {
    /* Autogen by nara studio */

    public void execute(DocQueryRequest<SoftwareEditionDoc> request) {
        /* Autogen by nara studio */
        request.addQueryParamsAndClass(getQueryParams(), SoftwareEditionDoc.class);
        Query query = DocQueryBuilder.build(request, getOffset());
        List<SoftwareEditionDoc> softwareEditionDocs = request.findAll(query);
        setResponse(Optional.ofNullable(softwareEditionDocs).map(docs -> SoftwareEditionDoc.toDomains(docs)).orElse(new ArrayList<>()));
        if (nonNull(getOffset()) && getOffset().isTotalCountRequested()) {
            Query totalCountQuery = DocQueryBuilder.build(request);
            long totalCount = request.count(totalCountQuery);
            Offset countableOffset = getOffset();
            countableOffset.setTotalCount((int) totalCount);
        }
    }
}
