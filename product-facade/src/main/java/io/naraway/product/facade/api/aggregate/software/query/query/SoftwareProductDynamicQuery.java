/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.software.query.query;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.DynamicQueryRequest;
import io.naraway.accent.util.query.DocQueryBuilder;
import io.naraway.accent.util.query.DocQueryRequest;
import io.naraway.product.aggregate.ProductDramaRole;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import io.naraway.product.aggregate.software.store.mongo.odm.SoftwareProductDoc;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(ProductDramaRole.Director)
public class SoftwareProductDynamicQuery extends DynamicQueryRequest<SoftwareProduct> {
    /* Autogen by nara studio */

    public void execute(DocQueryRequest<SoftwareProductDoc> request) {
        /* Autogen by nara studio */
        request.addQueryParamsAndClass(getQueryParams(), SoftwareProductDoc.class);
        Query query = DocQueryBuilder.build(request);
        SoftwareProductDoc softwareProductDoc = request.findOne(query);
        setResponse(Optional.ofNullable(softwareProductDoc).map(doc -> doc.toDomain()).orElse(null));
    }
}
