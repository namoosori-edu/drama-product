/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.software.query.query;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.QueryRequest;
import io.naraway.product.aggregate.ProductDramaRole;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import io.naraway.product.aggregate.software.store.SoftwareProductStore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(ProductDramaRole.Director)
public class SoftwareProductQuery extends QueryRequest<SoftwareProduct> {
    /* Autogen by nara studio */
    private String softwareProductId;

    public void execute(SoftwareProductStore softwareProductStore) {
        /* Autogen by nara studio */
        setResponse(softwareProductStore.retrieve(softwareProductId));
    }
}
