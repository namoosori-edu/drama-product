/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.software.query.rest;

import io.naraway.accent.domain.trail.QueryResponse;
import io.naraway.product.aggregate.software.domain.entity.SoftwareEdition;
import io.naraway.product.facade.api.aggregate.software.query.query.SoftwareEditionDynamicQuery;
import io.naraway.product.facade.api.aggregate.software.query.query.SoftwareEditionQuery;
import io.naraway.product.facade.api.aggregate.software.query.query.SoftwareEditionsDynamicQuery;

import java.util.List;

public interface SoftwareEditionQueryFacade {
    /* Autogen by nara studio */
    QueryResponse<SoftwareEdition> execute(SoftwareEditionQuery softwareEditionQuery);

    QueryResponse<SoftwareEdition> execute(SoftwareEditionDynamicQuery softwareEditionDynamicQuery);

    QueryResponse<List<SoftwareEdition>> execute(SoftwareEditionsDynamicQuery softwareEditionsDynamicQuery);
}
