/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.software.query.rest;

import io.naraway.accent.domain.trail.QueryResponse;
import io.naraway.accent.util.query.DocQueryRequest;
import io.naraway.product.aggregate.software.domain.entity.SoftwareEdition;
import io.naraway.product.aggregate.software.store.SoftwareEditionStore;
import io.naraway.product.aggregate.software.store.mongo.odm.SoftwareEditionDoc;
import io.naraway.product.facade.api.aggregate.software.query.query.SoftwareEditionDynamicQuery;
import io.naraway.product.facade.api.aggregate.software.query.query.SoftwareEditionQuery;
import io.naraway.product.facade.api.aggregate.software.query.query.SoftwareEditionsDynamicQuery;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/aggregate/software")
public class SoftwareEditionQueryResource implements SoftwareEditionQueryFacade {
    /* Autogen by nara studio */
    private final SoftwareEditionStore softwareEditionStore;
    private final MongoTemplate mongoTemplate;

    public SoftwareEditionQueryResource(SoftwareEditionStore softwareEditionStore, MongoTemplate mongoTemplate) {
        /* Autogen by nara studio */
        this.softwareEditionStore = softwareEditionStore;
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    @PostMapping("/software-edition/query")
    public QueryResponse<SoftwareEdition> execute(@RequestBody SoftwareEditionQuery softwareEditionQuery) {
        /* Autogen by nara studio */
        softwareEditionQuery.execute(softwareEditionStore);
        return softwareEditionQuery.getResponse();
    }

    @Override
    @PostMapping("/software-edition/dynamic-single/query")
    public QueryResponse<SoftwareEdition> execute(@RequestBody SoftwareEditionDynamicQuery softwareEditionDynamicQuery) {
        /* Autogen by nara studio */
        DocQueryRequest<SoftwareEditionDoc> request = new DocQueryRequest<>(mongoTemplate);
        softwareEditionDynamicQuery.execute(request);
        return softwareEditionDynamicQuery.getResponse();
    }

    @Override
    @PostMapping("/software-edition/dynamic-multi/query")
    public QueryResponse<List<SoftwareEdition>> execute(@RequestBody SoftwareEditionsDynamicQuery softwareEditionsDynamicQuery) {
        /* Autogen by nara studio */
        DocQueryRequest<SoftwareEditionDoc> request = new DocQueryRequest<>(mongoTemplate);
        softwareEditionsDynamicQuery.execute(request);
        return softwareEditionsDynamicQuery.getResponse();
    }
}
