/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.software.query.rest;

import io.naraway.accent.domain.trail.QueryResponse;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import io.naraway.product.facade.api.aggregate.software.query.query.SoftwareProductDynamicQuery;
import io.naraway.product.facade.api.aggregate.software.query.query.SoftwareProductQuery;
import io.naraway.product.facade.api.aggregate.software.query.query.SoftwareProductsDynamicQuery;

import java.util.List;

public interface SoftwareProductQueryFacade {
    /* Autogen by nara studio */
    QueryResponse<SoftwareProduct> execute(SoftwareProductQuery softwareProductQuery);

    QueryResponse<SoftwareProduct> execute(SoftwareProductDynamicQuery softwareProductDynamicQuery);

    QueryResponse<List<SoftwareProduct>> execute(SoftwareProductsDynamicQuery softwareProductsDynamicQuery);
}
