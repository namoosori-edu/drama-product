/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.api.aggregate.software.query.rest;

import io.naraway.accent.domain.trail.QueryResponse;
import io.naraway.accent.util.query.DocQueryRequest;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import io.naraway.product.aggregate.software.store.SoftwareProductStore;
import io.naraway.product.aggregate.software.store.mongo.odm.SoftwareProductDoc;
import io.naraway.product.facade.api.aggregate.software.query.query.SoftwareProductDynamicQuery;
import io.naraway.product.facade.api.aggregate.software.query.query.SoftwareProductQuery;
import io.naraway.product.facade.api.aggregate.software.query.query.SoftwareProductsDynamicQuery;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/aggregate/software")
public class SoftwareProductQueryResource implements SoftwareProductQueryFacade {
    /* Autogen by nara studio */
    private final SoftwareProductStore softwareProductStore;
    private final MongoTemplate mongoTemplate;

    public SoftwareProductQueryResource(SoftwareProductStore softwareProductStore, MongoTemplate mongoTemplate) {
        /* Autogen by nara studio */
        this.softwareProductStore = softwareProductStore;
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    @PostMapping("/software-product/query")
    public QueryResponse<SoftwareProduct> execute(@RequestBody SoftwareProductQuery softwareProductQuery) {
        /* Autogen by nara studio */
        softwareProductQuery.execute(softwareProductStore);
        return softwareProductQuery.getResponse();
    }

    @Override
    @PostMapping("/software-product/dynamic-single/query")
    public QueryResponse<SoftwareProduct> execute(@RequestBody SoftwareProductDynamicQuery softwareProductDynamicQuery) {
        /* Autogen by nara studio */
        DocQueryRequest<SoftwareProductDoc> request = new DocQueryRequest<>(mongoTemplate);
        softwareProductDynamicQuery.execute(request);
        return softwareProductDynamicQuery.getResponse();
    }

    @Override
    @PostMapping("/software-product/dynamic-multi/query")
    public QueryResponse<List<SoftwareProduct>> execute(@RequestBody SoftwareProductsDynamicQuery softwareProductsDynamicQuery) {
        /* Autogen by nara studio */
        DocQueryRequest<SoftwareProductDoc> request = new DocQueryRequest<>(mongoTemplate);
        softwareProductsDynamicQuery.execute(request);
        return softwareProductsDynamicQuery.getResponse();
    }
}
