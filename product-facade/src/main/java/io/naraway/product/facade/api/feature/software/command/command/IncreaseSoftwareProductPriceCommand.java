/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.facade.api.feature.software.command.command;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.CommandRequest;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.product.aggregate.ProductDramaRole;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.Assert;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@AuthorizedRole({ProductDramaRole.Director, ProductDramaRole.User})
public class IncreaseSoftwareProductPriceCommand extends CommandRequest {
    //
    private String skuNo;
    private double increaseRate;

    public void validate() {
        //
        Assert.hasText(skuNo, "'skuNo' is required");
        Assert.isTrue(increaseRate > 0, "'increaseRate' is greater than 0");
    }

    @Override
    public String toString() {
        //
        return toPrettyJson();
    }

    public static IncreaseSoftwareProductPriceCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, IncreaseSoftwareProductPriceCommand.class);
    }

    public static IncreaseSoftwareProductPriceCommand sample() {
        //
        return new IncreaseSoftwareProductPriceCommand(
                SoftwareProduct.sample().getSkuNo(),
                0.2
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
