/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.facade.api.feature.software.command.rest;

import io.naraway.accent.domain.trail.CommandResponse;
import io.naraway.product.facade.api.feature.software.command.command.IncreaseSoftwareProductPriceCommand;

public interface SoftwareFlowFacade {
    //
    CommandResponse increaseSoftwareProductPrice(IncreaseSoftwareProductPriceCommand command);
}
