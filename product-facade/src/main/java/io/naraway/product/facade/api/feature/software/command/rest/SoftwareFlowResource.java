/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.facade.api.feature.software.command.rest;

import io.naraway.accent.domain.trail.CommandResponse;
import io.naraway.product.facade.api.feature.software.command.command.IncreaseSoftwareProductPriceCommand;
import io.naraway.product.feature.software.flow.SoftwareFlow;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/feature/software")
@RequiredArgsConstructor
public class SoftwareFlowResource implements SoftwareFlowFacade {
    //
    private final SoftwareFlow softwareFlow;

    @Override
    @PostMapping("/increase-software-product-price/command")
    public CommandResponse increaseSoftwareProductPrice(@RequestBody IncreaseSoftwareProductPriceCommand command) {
        //
        command.validate();
        String skuNo = command.getSkuNo();
        double increaseRate = command.getIncreaseRate();

        String entityId = softwareFlow.increaseSoftwareProductPrice(skuNo, increaseRate);
        command.setResponse(entityId);
        return command.getResponse();
    }
}
