/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.facade.api.feature.software.query.query;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.QueryRequest;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.product.aggregate.ProductDramaRole;
import io.naraway.product.aggregate.category.domain.entity.Category;
import io.naraway.product.feature.software.domain.sdo.CategorizedSoftwareProductRdo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.Assert;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@AuthorizedRole({ProductDramaRole.Director, ProductDramaRole.User})
public class FindSoftwareProductsByCategoryQuery extends QueryRequest<List<CategorizedSoftwareProductRdo>> {
    //
    private String categoryId;

    public void validate() {
        //
        Assert.hasText(categoryId, "'categoryId' is required");
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static FindSoftwareProductsByCategoryQuery fromJson(String json) {
        //
        return JsonUtil.fromJson(json, FindSoftwareProductsByCategoryQuery.class);
    }

    public static FindSoftwareProductsByCategoryQuery sample() {
        //
        return new FindSoftwareProductsByCategoryQuery(
                Category.sample().getId()
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
