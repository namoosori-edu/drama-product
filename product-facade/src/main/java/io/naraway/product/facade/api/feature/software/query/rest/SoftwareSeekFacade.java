/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.facade.api.feature.software.query.rest;

import io.naraway.accent.domain.trail.QueryResponse;
import io.naraway.product.facade.api.feature.software.query.query.FindSoftwareProductsByCategoryQuery;
import io.naraway.product.feature.software.domain.sdo.CategorizedSoftwareProductRdo;

import java.util.List;

public interface SoftwareSeekFacade {
    //
    QueryResponse<List<CategorizedSoftwareProductRdo>> findSoftwareProductsByCategory(FindSoftwareProductsByCategoryQuery query);
}
