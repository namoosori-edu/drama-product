/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.facade.api.feature.software.query.rest;

import io.naraway.accent.domain.trail.QueryResponse;
import io.naraway.product.facade.api.feature.software.query.query.FindSoftwareProductsByCategoryQuery;
import io.naraway.product.feature.software.domain.sdo.CategorizedSoftwareProductRdo;
import io.naraway.product.feature.software.flow.SoftwareSeek;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/feature/software")
@RequiredArgsConstructor
public class SoftwareSeekResource implements SoftwareSeekFacade {
    //
    private final SoftwareSeek softwareSeek;

    @Override
    @PostMapping("/find-software-products-by-category/query")
    public QueryResponse<List<CategorizedSoftwareProductRdo>> findSoftwareProductsByCategory(@RequestBody FindSoftwareProductsByCategoryQuery query) {
        //
        query.validate();
        String categoryId = query.getCategoryId();

        List<CategorizedSoftwareProductRdo> categorizedSoftwareProducts = softwareSeek.findSoftwareProductsByCategory(categoryId);
        query.setResponse(categorizedSoftwareProducts);
        return query.getResponse();
    }
}
