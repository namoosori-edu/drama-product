/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.facade.event.listener.data;

import io.naraway.janitor.aspect.annotation.EventHandler;
import io.naraway.janitor.proxy.EventProxy;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import io.naraway.product.aggregate.software.domain.event.SoftwareProductEvent;
import io.naraway.product.event.software.SoftwareProductRegisteredEvent;
import io.naraway.product.event.software.SoftwareProductRemovedEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
@Transactional
@RequiredArgsConstructor
public class SoftwareProductEventHandlers {
    //
    private final EventProxy eventProxy;

    @SuppressWarnings("java:S131")
    @EventHandler
    public void on(SoftwareProductEvent event) {
        //
        SoftwareProduct softwareProduct = event.getSoftwareProduct();

        switch (event.getDataEventType()) {
            case Registered:
                eventProxy.publishEvent(new SoftwareProductRegisteredEvent(softwareProduct.getSkuNo()));
                break;
            case Removed:
                eventProxy.publishEvent(new SoftwareProductRemovedEvent(softwareProduct.getSkuNo()));
                break;
        }
    }
}
