/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.product.facade.event.projection;

import io.naraway.accent.domain.trail.wrapper.StreamEvent;
import io.naraway.product.aggregate.category.domain.event.CategoryEvent;
import io.naraway.product.aggregate.category.domain.logic.CategoryLogic;
import io.naraway.product.aggregate.software.domain.event.SoftwareEditionEvent;
import io.naraway.product.aggregate.software.domain.event.SoftwareProductEvent;
import io.naraway.product.aggregate.software.domain.logic.SoftwareEditionLogic;
import io.naraway.product.aggregate.software.domain.logic.SoftwareProductLogic;

public class ProjectionHandler {
    private final CategoryLogic categoryLogic; // Autogen by nara studio
    private final SoftwareProductLogic softwareProductLogic;
    private final SoftwareEditionLogic softwareEditionLogic;

    public ProjectionHandler(CategoryLogic categoryLogic, SoftwareProductLogic softwareProductLogic, SoftwareEditionLogic softwareEditionLogic) {
        /* Autogen by nara studio */
        this.categoryLogic = categoryLogic;
        this.softwareProductLogic = softwareProductLogic;
        this.softwareEditionLogic = softwareEditionLogic;
    }

    @SuppressWarnings("java:S131")
    public void handle(StreamEvent event) {
        /* Autogen by nara studio */
        String classFullName = event.getPayloadClass();
        String payload = event.getPayload();
        String eventName = classFullName.substring(classFullName.lastIndexOf(".") + 1);
        switch (eventName) {
            case "CategoryEvent":
                CategoryEvent categoryEvent = CategoryEvent.fromJson(payload);
                categoryLogic.handleEventForProjection(categoryEvent);
                break;
            case "SoftwareProductEvent":
                SoftwareProductEvent softwareProductEvent = SoftwareProductEvent.fromJson(payload);
                softwareProductLogic.handleEventForProjection(softwareProductEvent);
                break;
            case "SoftwareEditionEvent":
                SoftwareEditionEvent softwareEditionEvent = SoftwareEditionEvent.fromJson(payload);
                softwareEditionLogic.handleEventForProjection(softwareEditionEvent);
                break;
        }
    }
}
