/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.feature.software.action;

import io.naraway.janitor.proxy.EventProxy;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import io.naraway.product.aggregate.software.store.SoftwareProductStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SoftwareProductAction {
    //
    private final SoftwareProductStore softwareProductStore; //
    private final EventProxy eventProxy;

    public SoftwareProductAction(SoftwareProductStore softwareProductStore, EventProxy eventProxy) {
        /* Autogen by nara studio */
        this.softwareProductStore = softwareProductStore;
        this.eventProxy = eventProxy;
    }

    public List<SoftwareProduct> findByCategoryId(String categoryId) {
        /* Autogen by nara studio */
        return softwareProductStore.retrieveByCategoryId(categoryId);
    }
}
