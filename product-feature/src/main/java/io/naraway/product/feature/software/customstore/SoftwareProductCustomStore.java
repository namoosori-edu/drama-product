/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.feature.software.customstore;

import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;

import java.util.List;

public interface SoftwareProductCustomStore {
    //
    List<SoftwareProduct> retrieveAllByCategoryId(String categoryId);
}
