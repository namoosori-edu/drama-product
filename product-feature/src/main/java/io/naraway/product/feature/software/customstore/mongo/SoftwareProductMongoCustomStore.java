/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.feature.software.customstore.mongo;

import io.naraway.accent.domain.key.stage.ActorKey;
import io.naraway.drama.prologue.spacekeeper.support.DramaRequestContext;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import io.naraway.product.aggregate.software.store.mongo.odm.SoftwareProductDoc;
import io.naraway.product.feature.software.customstore.SoftwareProductCustomStore;
import io.naraway.product.feature.software.customstore.mongo.repository.SoftwareProductMongoCustomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class SoftwareProductMongoCustomStore implements SoftwareProductCustomStore {
    //
    private final SoftwareProductMongoCustomRepository softwareProductMongoCustomRepository;

    @Override
    public List<SoftwareProduct> retrieveAllByCategoryId(String categoryId) {
        //
        String cineroomId = ActorKey.fromId(DramaRequestContext.current().getActorId()).genCineroomId();
        return SoftwareProductDoc.toDomains(softwareProductMongoCustomRepository.findAllByCategoryIdAndRequesterCineroomId(categoryId, cineroomId));
    }
}
