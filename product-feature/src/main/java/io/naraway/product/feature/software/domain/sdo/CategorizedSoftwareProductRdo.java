/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.feature.software.domain.sdo;

import io.naraway.accent.util.json.JsonSerializable;
import io.naraway.product.aggregate.category.domain.entity.Category;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CategorizedSoftwareProductRdo implements JsonSerializable {
    //
    private SoftwareProduct softwareProduct;
    private Category category;

    public static CategorizedSoftwareProductRdo sample() {
        //
        return new CategorizedSoftwareProductRdo(SoftwareProduct.sample(), Category.sample());
    }

    public static void main(String[] args) {
        //
        log.debug(sample().toPrettyJson());
    }

    @Override
    public String toString() {
        //
        return toJson();
    }
}
