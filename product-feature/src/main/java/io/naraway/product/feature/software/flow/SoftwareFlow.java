/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.feature.software.flow;

import io.naraway.accent.domain.type.NameValueList;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import io.naraway.product.aggregate.software.domain.entity.vo.Price;
import io.naraway.product.aggregate.software.domain.logic.SoftwareProductLogic;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class SoftwareFlow {
    //
    private final SoftwareProductLogic softwareProductLogic;

    public String increaseSoftwareProductPrice(String skuNo, double increaseRate) {
        //
        SoftwareProduct softwareProduct = softwareProductLogic.findBySkuNo(skuNo);
        Price price = softwareProduct.getPrice();
        price.setSalesPrice((long) (price.getSalesPrice() * increaseRate));

        softwareProductLogic.modifySoftwareProduct(softwareProduct.getId(),
                NameValueList.newInstance("price", price.toJson()));

        return softwareProduct.getId();
    }
}
