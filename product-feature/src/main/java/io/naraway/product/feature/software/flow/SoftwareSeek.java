/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
 */
package io.naraway.product.feature.software.flow;

import io.naraway.product.aggregate.category.domain.entity.Category;
import io.naraway.product.aggregate.category.domain.logic.CategoryLogic;
import io.naraway.product.aggregate.software.domain.entity.SoftwareProduct;
import io.naraway.product.feature.software.action.SoftwareProductAction;
import io.naraway.product.feature.software.domain.sdo.CategorizedSoftwareProductRdo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class SoftwareSeek {
    //
    private final SoftwareProductAction softwareProductAction;
    private final CategoryLogic categoryLogic;

    public List<CategorizedSoftwareProductRdo> findSoftwareProductsByCategory(String cateoryId) {
        //
        List<SoftwareProduct> softwareProducts = softwareProductAction.findByCategoryId(cateoryId);
        return softwareProducts.stream()
                .map(product -> {
                    CategorizedSoftwareProductRdo categorizedSoftwareProductRdo = new CategorizedSoftwareProductRdo();
                    BeanUtils.copyProperties(product, categorizedSoftwareProductRdo);
                    Category category = categoryLogic.findCategory(product.getCategoryId());
                    categorizedSoftwareProductRdo.setCategory(category);

                    return categorizedSoftwareProductRdo;
                })
                .collect(Collectors.toList());
    }
}
